#!/bin/bash

# copy start scripts
sudo cp dwm.desktop /usr/share/xsessions/dwm.desktop
sudo cp dwm-personalized /usr/local/bin/dwm-personalized
sudo chmod +x /usr/local/bin/dwm-personalized

# create wallpaper dir
sudo mkdir -p /usr/local/share/dwm-personalized/

# other configs
#cp other_configs/nanorc /home/$(whoami)/.nanorc
#cp other_configs/muttrc /home/$(whoami)/.muttrc
echo "Finished!"
echo "Config files from other_configs folder needs to be installed manually..."
