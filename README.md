DWM
===

where to copy/link files:

    sudo cp dwm.desktop /usr/share/xsessions/dwm.desktop
    sudo cp dwm-personalized /usr/local/bin/dwm-personalized
    cp config.h PATH/TO/SOURCE/config.h

Give execute permissions:
    
    sudo chmod +x /usr/local/bin/dwm-personalized

Put wallpaper to:

    sudo cp wallpaper.png /usr/local/share/dwm-personalized/dwm-wallpaper.png

Do not forget to give it read permissions:

    sudo chmod 444 /usr/local/share/dwm-personalized/dwm-wallpaper.png

then build and install dwm with: (from source dir)

    sudo make clean install

Other configs
-------------

nano

    /home/$(whoami)/.nanorc
